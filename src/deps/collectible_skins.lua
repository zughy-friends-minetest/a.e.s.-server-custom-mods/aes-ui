collectible_skins.register_on_set_skin(function(p_name, skin_ID)
    if not parties.is_player_in_party(p_name) then return end

    aes_ui.hud_party_update(p_name, "portrait")
end)