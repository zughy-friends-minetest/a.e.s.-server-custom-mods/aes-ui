arena_lib.register_on_load(function(mod, arena)
  for pl_name, _ in pairs(arena.players) do
    aes_ui.hud_hide(pl_name)
    aes_ui.hud_party_update(pl_name, "minigame")
  end
end)



arena_lib.register_on_join(function(mod, arena, p_name, as_spectator, was_spectator)
  aes_ui.hud_hide(p_name)
  aes_ui.hud_party_update(p_name, "minigame")
end)



arena_lib.register_on_quit(function(mod, arena, p_name, is_spectator, reason)
  aes_ui.hud_show(p_name)
  aes_ui.hud_party_update(p_name, "minigame")
end)



arena_lib.register_on_end(function(mod, arena, winners, is_forced)
  for psp_name, _ in pairs(arena.players_and_spectators) do
    aes_ui.hud_party_update(psp_name, "minigame")
    aes_ui.hud_show(psp_name)
  end
end)