parties.register_on_party_join(function(party_leader, p_name)
  aes_ui.hud_party_create(p_name)
end)



parties.register_on_party_leave(function(party_leader, p_name, reason)
  if reason ~= 3 then
    aes_ui.hud_party_remove(p_name)
  elseif p_name == party_leader then
    aes_ui.hud_party_remove_all(p_name) -- se il gruppo viene sciolto
  end
end)