function aes_ui.hud_show(p_name)
  if parties.is_player_in_party(p_name) then
    for _, pl_name in pairs(parties.get_party_members(p_name)) do
      local panel = panel_lib.get_panel(p_name, "aes_party_" .. pl_name)
      panel:show()
    end
  end
end



function aes_ui.hud_hide(p_name)
  if parties.is_player_in_party(p_name) then
    for _, pl_name in pairs(parties.get_party_members(p_name)) do
      local panel = panel_lib.get_panel(p_name, "aes_party_" .. pl_name)
      panel:hide()
    end
  end
end