local function add_slot() end
local function calc_minigame() end
local function get_portrait() end



function aes_ui.hud_party_create(p_name, reconnected)
  local pt_members = parties.get_party_members(p_name)
  local y_offset = 0.24

  for _, pl_name in ipairs(pt_members) do
    add_slot(p_name, pl_name, y_offset)
    y_offset = y_offset + 0.1
  end

  -- se la squadra si è appena formata o è unə giocatorə riconnessəsi, non c'è niente da aggiungere
  if #pt_members == 2 or reconnected then return end

  -- sennò aggiungo il nuovo pannello a chi era già nel gruppo
  for _, pl_name in ipairs(pt_members) do
    if p_name ~= pl_name and minetest.get_player_by_name(pl_name) then
      add_slot(pl_name, p_name, y_offset - 0.1) -- y_offset era pronta ad assegnare un'eventuale casella successiva, quindi arretro di 1 posizione
    end
  end
end



function aes_ui.hud_party_remove(p_name)
  local pt_members = parties.get_party_members(p_name)
  local pt_members_online = parties.get_party_members(p_name, true)
  local p_id
  local is_p_online = minetest.get_player_by_name(p_name)

  -- tolgo tutti i pannelli alla persona rimossa, se ancora connessa. Sennò ottengo
  -- semplicemente la sua posizione nel gruppo
  for i, pl_name in ipairs(pt_members) do
    if is_p_online then
      local p_panel = panel_lib.get_panel(p_name, "aes_party_" .. pl_name)
      p_panel:remove()
    end

    -- ottengo la sua posizione nel gruppo, per capire se dopo ci sono da spostare
    -- pannelli (quelli che seguono). Ciclo anche tra giocatorɜ non connessɜ (idem
    -- sotto) perché sennò se rimuovessi #2 con lə #3 disconnessə, #3 non verrebbe
    -- spostatə
    if p_name == pl_name then
      p_id = i
    end
  end

  -- rimuovo il pannello della persona rimossa
  for i, pl_name in ipairs(pt_members) do
    if pl_name ~= p_name then
      -- devo controllare manualmente perché nel ciclo dopo (pla_panel) mi servono
      -- anche lɜ giocatorɜ non connessɜ
      if minetest.get_player_by_name(pl_name) then
        local pl_panel = panel_lib.get_panel(pl_name, "aes_party_" .. p_name)
        pl_panel:remove()
      end

      -- sposto in alto i pannelli che seguivano
      if p_id < i then
        for _, pla_name in ipairs(pt_members_online) do
          if pla_name ~= p_name then
            local pla_panel = panel_lib.get_panel(pla_name, "aes_party_" .. pl_name)
            pla_panel:update({position = { x = 0, y = pla_panel:get_info().bg.position.y - 0.1}})
          end
        end
      end
    end
  end
end



-- rimuove le caselle senza fare calcoli vari
function aes_ui.hud_party_remove_all(p_name)
  local pt_members_online = parties.get_party_members(p_name, true)
  local pt_members = parties.get_party_members(p_name)

  for _, pl_name in ipairs(pt_members_online) do
    for _, pla_name in pairs(pt_members) do
      local p_panel = panel_lib.get_panel(pl_name, "aes_party_" .. pla_name)
      p_panel:remove()
    end
  end
end



function aes_ui.hud_party_update(p_name, param)
  if not parties.is_player_in_party(p_name) then return end

  if param == "minigame" then
    local icon = calc_minigame(p_name)

    for _, pl_name in ipairs(parties.get_party_members(p_name, true)) do
      local panel = panel_lib.get_panel(pl_name, "aes_party_" .. p_name)
      panel:update(nil, nil, {mg_icon = {text = icon}})
    end

  elseif param == "portrait" then
    local portrait = get_portrait(collectible_skins.get_player_skin(p_name))

    for _, pl_name in ipairs(parties.get_party_members(p_name, true)) do
      local panel = panel_lib.get_panel(pl_name, "aes_party_" .. p_name)
      panel:update(nil, nil, {portrait = {text = portrait}})
    end

  elseif param == "online" then
    -- after per controllare se si è disconness@
    minetest.after(0.1, function()
      local icon, bg

      if minetest.get_player_by_name(p_name) then
        bg = parties.is_player_party_leader(p_name) and "party_hud_bg_leader.png" or "party_hud_bg.png"
        icon = ""
      else
        bg = "party_hud_bg_offline.png"
        icon = "party_hud_portrait_offline.png"
      end

      for _, pl_name in ipairs(parties.get_party_members(p_name, true)) do
        local panel = panel_lib.get_panel(pl_name, "aes_party_" .. p_name)
        panel:update({bg = bg}, nil, {offline = {text  = icon}})
      end
    end)
  end
end




----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function add_slot(p_name, t_name, y_offset)
  local name = string.len(t_name) < 13 and t_name or string.sub(t_name, 1, 12) .. "..."
  local portrait = get_portrait(collectible_skins.get_player_skin(t_name))
  local offline = minetest.get_player_by_name(t_name) and "" or "party_hud_portrait_offline.png"
  local visible = not arena_lib.is_player_in_arena(p_name)
  local bg, crown

  if parties.is_player_party_leader(t_name) then
    bg = "party_hud_bg_leader.png"
    crown = "party_hud_leader.png"
  else
    bg = "party_hud_bg.png"
    crown = ""
  end

  Panel:new("aes_party_" .. t_name, {
    player = p_name,
    bg = bg,
    bg_scale = { x = 2.2, y = 2.2},
    position = { x = 0, y = y_offset },
    offset = { x = 6, y = 0},
    alignment = { x = 1, y = 0 },
    title_alignment = { x = 1, y = 0 },
    title_offset = { x = 80, y = -13 },
    title = name,
    visible = visible,

    sub_img_elems = {
      portrait = {
        scale   = { x = 6, y = 6 },
        offset  = { x = -26 },
        text    = portrait
      },
      offline = {
        scale   = { x = 3.1, y = 3.1 },
        offset  = { x = 23 },
        text    = offline,
        z_index = 1
      },
      overlay = {
        text = "party_hud_overlay.png",
        z_index = 2
      },
      leader = {
        text = crown,
        z_index = 3
      },
      mg_icon = {
        text = calc_minigame(t_name),
        offset = {x = 75, y = 13},
        scale = {x = 1.5, y = 1.5}
      }
    },
  })
end



function calc_minigame(p_name)
  local mod = arena_lib.get_mod_by_player(p_name)
  local icon = ""

  if mod and arena_lib.is_player_in_arena(p_name) then
    icon = arena_lib.mods[mod].icon

    if arena_lib.is_player_spectating(p_name) then
      icon = icon .. "^[multiply:#888888" -- TEMP: MT 5.8, usa [hsl:<hue>:<saturation>:<lightness> con saturazione 0 e illu un filo più bassa
    end
  end

  return icon
end



function get_portrait(skin)
  -- TEMP: non è possibile ottenere gli aspetti di chi non è connessə al momento
  -- (LT non permette di accedere ai metadati di chi è offline). Quindi passo blank.png in caso
  return skin and "([combine:24x24:0,0=" .. skin.texture .. "^[mask:party_portrait_mask.png)" or "blank.png"
end