minetest.register_on_joinplayer(function(player)
  local p_name = player:get_player_name()

  if parties.is_player_in_party(p_name) then
    aes_ui.hud_party_create(p_name, true)
    aes_ui.hud_party_update(p_name, "portrait") -- TEMP, vedasi get_portrait()
    aes_ui.hud_party_update(p_name, "online")
  end
end)

minetest.register_on_leaveplayer(function(player)
  aes_ui.hud_party_update(player:get_player_name(), "online")
end)