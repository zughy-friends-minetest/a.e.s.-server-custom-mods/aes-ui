local srcpath = minetest.get_modpath("aes_ui") .. "/src"

aes_ui = {}

dofile(srcpath .. "/deps/arena_lib.lua")
dofile(srcpath .. "/deps/collectible_skins.lua")
dofile(srcpath .. "/deps/parties.lua")
dofile(srcpath .. "/HUD/hud.lua")
dofile(srcpath .. "/HUD/hud_party.lua")
dofile(srcpath .. "/player_manager.lua")